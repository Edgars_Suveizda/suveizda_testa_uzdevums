<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link rel="stylesheet" href="style.css" type="text/css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script
        <script src="main.js" type="text/javascript"></script>
    </head>
    <body>
        <?php
            require_once "conect_db.php";//pieslēdzamies datubāzei
            require_once "product.php";//piesledzam klases failu
            require_once "main.php";
            
        ?>
       <div class="container">
           <div class="page-heading">Product List</div>
           <form class="edit-form delete-form" method="post" action="<?$_SERVER['SCRIPT_NAME']?>" enctype="application/x-www-form-urlencoded">
               <div class="input-row">
                   <div class="product-wrapper">
               <?
                    $product_list = getProducts($connect);
                    foreach ($product_list as $product){
                   
               ?>
                       <a href="#" class="product-item">
                           <div class="product-sku">
                               <!--
                               checkbox elements, kurš būs jāatzīmē, ja lietotājs vēlas šo produktu dzēst,
                               visi atzīmētie elementi tiek saglabāti masīvā
                               *-->
                               <input type="checkbox" name="ready_to_delete[]" value="<?=$product->getSku()?>">
                               <?echo $product->getSku();?>
                           </div>
                           <div class="product-name"><?echo $product->getName();?></div>
                           <div class="product-price"><?echo $product->getPrice();?> &euro;</div>
                           <div class="product-attributes">
                               <?
                                    //atkarībā no produkta tipa izvadām attiecīgo informāciju
                                    getAttrib($product);
                               ?>
                           </div>
                       </a>
               <?
                    }
               ?>
           </div>
               </div>
               <div class="input-row">
                   <input type="submit" name="mass_delete" value="Delete selected products">
                   <?
                        //ja ir nospiesta poga un ir atzīmēts kaut viens produkts , atzīmētais produkts/produkti
                        //tiek dzēsti no datubāzes
                        if(isset($_REQUEST['mass_delete'])){
                            if(count($_REQUEST['ready_to_delete'])>=0) {
                                massDelete($_REQUEST['ready_to_delete'], $connect);
                                unset($product_list);
                                header("Location:index.php");
                            }
                            else{
                                unset($product_list);
                                header("Location:index.php");
                            }
                        }
                   ?>
               </div>
           </form>
           <div class="btn-wrapper">
               <a href="add-product.php" class="btn">Add Product</a>
           </div>
       </div>
    </body>
</html>