<?php
    //funkcija kura galvenajā failā atgriež visu produktu masīvu
    function getProducts($connect){
        //dabūjam no datubāzes visus unikālos identifikatorus
        $q_text = "SELECT SKU, Type FROM Product_list";
        $result = mysqli_query($connect,$q_text);
        $products_arr = array();
        while ($result_row = mysqli_fetch_array($result)) {
            switch ($result_row['Type']) {
                case "furniture":
                    $products_arr[] = new Furniture($result_row['SKU'], $connect);
                    break;
                case "disc":
                    $products_arr[] = new Disc($result_row['SKU'], $connect);
                    break;
                case "book":
                    $products_arr[] = new Book($result_row['SKU'], $connect);
                    break;

            }
        }

        return $products_arr;
    }

    ///funkcija, kura izvada attiecīgā produkta atribūtus
    function getAttrib($product){
        $product_type = $product->getType();
        if($product_type=="furniture") {
            $dimensions_arr=$product->getDimensions();

            echo"<span>Width:". $dimensions_arr[0]."mm</span>
            <span>Length:". $dimensions_arr[1]."mm</span>
            <span>Height:".$dimensions_arr[2]."mm</span>";

        }
        elseif($product_type=="book"){

            echo "<span>Weight:". $product->getWeight()." kg</span>";

        }
        elseif($product_type=="disc"){
            echo "<span>Size:". $product->getSize()." MB</span>";
        }
    }
    ///funkcija kura veic masu džēšanu
    function massDelete($delete_arr, $connect){
        foreach ($delete_arr as $ready_to_delete){
            $delete_text = "DELETE FROM Product_list WHERE SKU = '".$ready_to_delete."'";
            mysqli_query($connect,$delete_text);
           

        }
    }
    ///funkcija, kura atgriež SKU masīvu
    function getSkuArr($connect){
        $q_text = "SELECT SKU FROM Product_list";
        $result = mysqli_query($connect,$q_text);
        $sku_arr = array();
        while($result_row = mysqli_fetch_array($result)){
            $sku_arr[]=$result_row['SKU'];
        }
        return $sku_arr;
    }

    //funkcija, kur pievieno produktu
    function addProduct($statement, $type, $name, $sku, $sku_arr,$price, $attribute, $connect){
        if($statement==true &&$type===""){
            echo"<span class=error-msg>All fields must be filled</span>";
        }
        //ja ir nospiesta poga un produkta tips ir mēbele, tad noslēptie formas lauki pieņem statiskās vērtības
        //ja atlikuši lauki nav norādīti izvadās attiecīgs paziņojums, ja viss ir ok tad tiek izsaukta attiecīgā funkcija
        //kura veic saglabāšanu datu bāzē
        elseif ($statement==true&&$type==="furniture"){
            if($sku!=""&&$name!=""&&$price!=NULL&&$attribute!=""){
                $counter = 0;
                foreach ($sku_arr as $sku_el){
                    if($sku===$sku_el){$counter++;}
                }
                if($counter==0) {
                    Furniture::insertDb($connect, $sku, $name, $price, $type,  $attribute);
                }
                else{

                    echo"<span class=error-msg>This SKU is already used, please try another one</span>";

                }
            }
            else{
                ?>
                echo"<span class=error-msg>All fields must be filled</span>";
                <?
            }
        }
        //tas pats darbības pricips, kas aprakstīts augstāk, tikai tips ir disks
        elseif ($statement==true&&$type==="disc"){
            if($sku!=""&&$name!=""&&$price!=NULL&&$attribute!=NULL){
                $counter = 0;
                foreach ($sku_arr as $sku_el){
                    if($sku===$sku_el){$counter++;}
                }
                if($counter==0) {
                    Disc::insertDb($connect, $sku, $name, $price, $type,  $attribute);
                }
                else{
                    echo"<span class=error-msg>This SKU is already used, please try another one</span>";
                }
            }
            else{
                echo"<span class=error-msg>All fields must be filled</span>";
            }
        }
        //tas pats princips , kas aprakstīts augstāk, tikai tips ir grāmata
        elseif ($statement==true&&$type==="book"){
            if($sku!=""&&$name!=""&&$price!=NULL&&$attribute!=NULL){
                $counter = 0;
                foreach ($sku_arr as $sku_el){
                    if($sku===$sku_el){$counter++;}
                }
                if($counter==0) {
                    Book::insertDb($connect, $sku, $name, $price, $type,  $attribute);
                }
                else{
                    echo"<span class=error-msg>This SKU is already used, please try another one</span>";
                }
            }
            else{
                echo"<span class=error-msg>All fields must be filled</span>";
            }
        }
    }
?>