<?php
    abstract class Product{
        //definējas bāzes klase, kurā tiek noteiktas tās produkta īpašības, kuras mantosies atlikušajās klasēs
        protected $sku;
        protected $price;
        protected $name;
        protected $type;

        public abstract function __construct($sku,$connection);


        public function __destruct()
        {

        }

        //funkcijas kas atgriež klases parametru vērtības
        public function getName(){return $this->name;}
        public function getSku(){return $this->sku;}
        public function getPrice(){return $this->price;}
        public function getType(){return $this->type;}
        
    }

    class Furniture extends Product{
        //nosakām īpašību, kas ir unikāla šai produktu grupai
        private $dimensions;

        public function __construct($sku, $connection){
            $this->sku = $sku;
            $query_text = "SELECT Name, Price, Type, Dimensions  FROM Product_list WHERE SKU = '".$sku."'";
            $query_result = mysqli_query($connection, $query_text);
            while ($result_row = mysqli_fetch_array($query_result)){
                $this->price = $result_row['Price'];
                $this->name = $result_row['Name'];
                $this->type = $result_row['Type'];
                $this->dimensions = $result_row['Dimensions'];
            }



        }


        //funkcija , kas atgriež mēbees dimensijas kā skaitļu masīvu
        public function getDimensions(){return explode("x", $this->dimensions);}

        //funkcija kura ievieto informāciju no formas datubāzē izvairoties no SQL injekcijām
        public static function insertDb($connection, $sku, $name, $price, $type, $dimensions){
            $insert_string = "INSERT INTO Product_list (SKU, Name, Price, Type,  Dimensions)
            VALUES('".mysqli_real_escape_string($connection,$sku)."','".mysqli_real_escape_string($connection,$name)."',
            ".mysqli_real_escape_string($connection,$price).",'".mysqli_real_escape_string($connection,$type)."',
            '".mysqli_real_escape_string($connection,$dimensions)."')";
            mysqli_query($connection,$insert_string);
        }
    }

    class Disc extends Product{
        //nosakām īpašību, kas ir unikāla šai produktu grupai
        private $size;

        public function __construct($sku, $connection){
            $this->sku = $sku;
            $query_text = "SELECT Name, Price, Type, Size_MB  FROM Product_list WHERE SKU = '".$sku."'";
            $query_result = mysqli_query($connection, $query_text);
            while ($result_row = mysqli_fetch_array($query_result)){
                $this->price = $result_row['Price'];
                $this->name = $result_row['Name'];
                $this->type = $result_row['Type'];
                $this->size = $result_row['Size_MB'];
            }



        }

        //funkcija , kas atgriež mēbees dimensijas kā skaitļu masīvu
        public function getSize(){return $this->size;}
    
        //funkcija kura ievieto informāciju no formas datubāzē izvairoties no SQL injekcijām
        public static function insertDb($connection, $sku, $name, $price, $type, $size){
            $insert_string = "INSERT INTO Product_list (SKU, Name, Price, Type,  Size_MB)
                VALUES('".mysqli_real_escape_string($connection,$sku)."','".mysqli_real_escape_string($connection,$name)."',
                ".mysqli_real_escape_string($connection,$price).",'".mysqli_real_escape_string($connection,$type)."',
                ".mysqli_real_escape_string($connection,$size).")";
            mysqli_query($connection,$insert_string);
        }
    }

    class Book extends Product{
        //nosakām īpašību, kas ir unikāla šai produktu grupai
        private $weight;

        public function __construct($sku, $connection){
            $this->sku = $sku;
            $query_text = "SELECT Name, Price, Type, Weight_Kg  FROM Product_list WHERE SKU = '".$sku."'";
            $query_result = mysqli_query($connection, $query_text);
            while ($result_row = mysqli_fetch_array($query_result)){
                $this->price = $result_row['Price'];
                $this->name = $result_row['Name'];
                $this->type = $result_row['Type'];
                $this->weight = $result_row['Weight_Kg'];
            }



        }
        
        //funkcija , kas atgriež mēbees dimensijas kā skaitļu masīvu
        public function getWeight(){return $this->weight;}

        //funkcija kura ievieto informāciju no formas datubāzē izvairoties no SQL injekcijām
        public static function insertDb($connection, $sku, $name, $price, $type, $weight){
            $insert_string = "INSERT INTO Product_list (SKU, Name, Price, Type,  Weight_Kg)
                VALUES('".mysqli_real_escape_string($connection,$sku)."','".mysqli_real_escape_string($connection,$name)."',
                ".mysqli_real_escape_string($connection,$price).",'".mysqli_real_escape_string($connection,$type)."',
                ".mysqli_real_escape_string($connection,$weight).")";
            mysqli_query($connection,$insert_string);
        }
    }
?>