<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link rel="stylesheet" href="style.css" type="text/css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
        <script src="main.js" type="text/javascript"></script>
    </head>
    <body>
        <?
            //pieslēdzam vajadzīgos failus, dabūjam unikālo atslēgu masīvu
            require_once "conect_db.php";
            require_once "product.php";
            require_once "main.php";
            $sku_arr = getSkuArr($connect);
        ?>
       <div class="container">
           <div class="page-heading">Add product</div>
           <form class="edit-form add-form" method="post" action="<?$_SERVER['SCRIPT_NAME']?>" enctype="application/x-www-form-urlencoded">
               <div class="input-row">
                   <input type="text" name="sku" placeholder="Enter product SKU">
               </div>
               <div class="input-row">
                   <input type="text" name="product_name" placeholder="Enter product name">
               </div>
               <div class="input-row">
                   <input type="text" name="price" placeholder="Enter product price">
               </div>
               <div class="input-row">
                   <select name="type" id="type" onchange="showInputs()">
                       <option value="">Select product type</option>
                       <option value="furniture">Furniture</option>
                       <option value="disc">Disc</option>
                       <option value="book">Book</option>
                   </select>
                   <script>
                       function showInputs() {


                           if (document.getElementById("type").value === "furniture") {
                               $(".for-furniture").addClass("active");
                               $(".for-disc").removeClass("active");
                               $(".for-book").removeClass("active");
                           }
                           else if (document.getElementById("type").value === "disc") {
                               $(".for-furniture").removeClass("active");
                               $(".for-disc").addClass("active");
                               $(".for-book").removeClass("active");
                           }
                           else if (document.getElementById("type").value === "book") {
                               $(".for-furniture").removeClass("active");
                               $(".for-disc").removeClass("active");
                               $(".for-book").addClass("active");
                           }
                       }
                   </script>
               </div>
               <div class="input-row for-furniture">
                   <input type="text" name="dimensions" placeholder="WxLxH">
                   <span class="describe-text">Please enter furniture dimensions like this : Width x Lenght x Height (example 100x60x80) Dimensions unit are millimetres</span>
               </div>
               <div class="input-row for-disc">
                   <input type="text" name="size" placeholder="700">
                   <span class="describe-text">Please enter disc size in MB (enter only number)</span>
               </div>
               <div class="input-row for-book">
                   <input type="text" name="weight" placeholder="5">
                   <span class="describe-text">Please enter book weight in kg (enter only number)</span>
               </div>
               <div class="input-row">
                   <input type="submit" value="Add product" name="save">
               </div>
           </form>
           <?
            //izsaucam pievienošanas funkciju, atribūta parametrā nosūtām vajadzīg lauka vertību atkarībā no izvēlētā produkta tipa
                switch ($_REQUEST['type']) {
                    case "furniture":
                        addProduct(isset($_REQUEST['save']), $_REQUEST['type'], $_REQUEST['product_name'], $_REQUEST['sku'], $sku_arr, $_REQUEST['price'], $_REQUEST['dimensions'], $connect);
                        break;
                    case "book":
                        addProduct(isset($_REQUEST['save']), $_REQUEST['type'], $_REQUEST['product_name'], $_REQUEST['sku'], $sku_arr, $_REQUEST['price'], $_REQUEST['weight'], $connect);
                        break;
                    case "disc":
                        addProduct(isset($_REQUEST['save']), $_REQUEST['type'], $_REQUEST['product_name'], $_REQUEST['sku'], $sku_arr, $_REQUEST['price'], $_REQUEST['size'], $connect);
                        break;
                }
           ?>

           <div class="btn-wrapper">
               <a href="index.php" class="btn">Back to list</a>
           </div>
       </div>
    </body>
</html>