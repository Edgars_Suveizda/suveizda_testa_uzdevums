-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 28, 2018 at 12:36 PM
-- Server version: 10.1.31-MariaDB-cll-lve
-- PHP Version: 5.6.30

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `suvisid_scan_test`
--
CREATE DATABASE IF NOT EXISTS `suvisid_scan_test` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `suvisid_scan_test`;

-- --------------------------------------------------------

--
-- Table structure for table `Product_list`
--

DROP TABLE IF EXISTS `Product_list`;
CREATE TABLE IF NOT EXISTS `Product_list` (
  `SKU` varchar(4) NOT NULL,
  `Name` text NOT NULL,
  `Price` double NOT NULL,
  `Type` text NOT NULL,
  `Size_MB` int(11) DEFAULT NULL,
  `Weight_Kg` int(11) DEFAULT NULL,
  `Dimensions` text,
  PRIMARY KEY (`SKU`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `Product_list`
--

TRUNCATE TABLE `Product_list`;
--
-- Dumping data for table `Product_list`
--

INSERT DELAYED IGNORE INTO `Product_list` (`SKU`, `Name`, `Price`, `Type`, `Size_MB`, `Weight_Kg`, `Dimensions`) VALUES
('BOO1', 'War and piece', 25.5, 'book', 0, 3, ''),
('BOO2', 'Harry potter', 12, 'book', 0, 12, ''),
('BOO3', 'MUmu', 15, 'book', NULL, NULL, '2'),
('DIS1', 'Acme Disc', 40.4, 'disc', 700, 0, ' '),
('DIS2', 'Google drive', 555, 'disc', 850, NULL, NULL),
('FUR1', 'Table', 99.99, 'furniture', 0, 0, '100x80x120'),
('FUR2', 'Chair', 40, 'furniture', NULL, NULL, '40x40x40');
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
